from django.urls import path
from .views import create_task, list_task


urlpatterns = [
    path("mine/", list_task, name="show_my_tasks"),
    path("create/", create_task, name="create_task"),
]
